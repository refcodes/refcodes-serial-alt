// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.AsciizSegment;
import org.refcodes.serial.Port;
import org.refcodes.serial.PortTestBench;
import org.refcodes.serial.SegmentResult;

public class TtyDataTypesTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testTransmitAsciizSegment1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final AsciizSegment theSegment = asciizSegment( "Hallo Welt!" );
				final AsciizSegment theOtherSegment = asciizSegment();
				final SegmentResult<AsciizSegment> theResult = theReceiverPort.onReceiveSegment( theOtherSegment );
				theSegment.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "The payload = " + theSegment.getPayload() );
					System.out.println( "The other payload = " + theOtherSegment.getPayload() );
				}
				assertEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
			}
		}
	}
}
