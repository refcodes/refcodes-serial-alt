// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Endianess;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.ComplexTypeSegment;
import org.refcodes.serial.Port;
import org.refcodes.serial.PortTestBench;
import org.refcodes.serial.Segment;
import org.refcodes.serial.SegmentResult;
import org.refcodes.serial.StopAndWaitPacketStreamSegmentDecorator;
import org.refcodes.serial.StringArraySection;
import org.refcodes.serial.StringSection;
import org.refcodes.serial.TransmissionMetrics;
import org.refcodes.serial.alt.tty.TestFixures.WeatherData;

/**
 *
 */
public class TtyPacketStreamTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOOPS = 3;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testStopAndWaitSegmentTransmitReceive1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 256 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringSection theOtherStringSection = new StringSection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
					System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
				}
				assertEquals( theStringSection, theOtherStringSection );
			}
		}
	}

	@Test
	public void testStopAndWaitSegmentTransmitReceive1Crc() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 256 ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringSection theOtherStringSection = new StringSection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
					System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
				}
				assertEquals( theStringSection, theOtherStringSection );
			}
		}
	}

	@Test
	public void testStopAndWaitSegmentTransmitReceive2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 0 ).withBlockSize( 5 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringArraySection theOtherStringSection = new StringArraySection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: " + Arrays.toString( theStringSection.getPayload() ) );
					System.out.println( "Other string section: " + Arrays.toString( theOtherStringSection.getPayload() ) );
				}
				assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
			}
		}
	}

	@Test
	public void testStopAndWaitSegmentTransmitReceive2Crc() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 0 ).withBlockSize( 5 ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringArraySection theOtherStringSection = new StringArraySection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: " + Arrays.toString( theStringSection.getPayload() ) );
					System.out.println( "Other string section: " + Arrays.toString( theOtherStringSection.getPayload() ) );
				}
				assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
			}
		}
	}

	@Test
	public void testStopAndWaitPacketStreamSegmentDecorator() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverComplexSegment;
				final Segment theSenderSeg = stopAndWaitPacketStreamSegmentBuilder().withDecoratee( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ).withBlockSize( 8 ).build();
				final Segment theReceiverSeg = stopAndWaitPacketStreamSegmentBuilder().withDecoratee( crcPrefixSegment( theReceiverComplexSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ) ).withBlockSize( 8 ).build();
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final WeatherData theReceiverData = theReceiverComplexSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
					thePortTestBench.waitShortestForPortCatchUp();
				}
			}
		}
	}
}
