// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoTimeout;
import org.refcodes.data.Text;
import org.refcodes.exception.Trap;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.Segment;
import org.refcodes.serial.SegmentResult;
import org.refcodes.serial.StringSection;

public class TtyPortHubTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String MESSAGE = Text.ARECIBO_MESSAGE.getText() + "\n" + Text.ARECIBO_MESSAGE.getText() + "\n" + Text.ARECIBO_MESSAGE.getText() + "\n" + Text.ARECIBO_MESSAGE.getText() + "\n" + Text.ARECIBO_MESSAGE.getText();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// SETUP:

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testListTtyPorts() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final TtyPortHub theHub = new TtyPortHub();
			final TtyPort[] thePorts = theHub.ports();
			for ( TtyPort ePort : thePorts ) {
				System.out.println( "-----> " + ePort.toString() );
			}
		}
	}

	@Test
	public void testListTtyPortsByPattern() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final TtyPortHub theHub = new TtyPortHub();
			final TtyPort[] thePorts = theHub.ports( "ttyUSB*" );
			for ( TtyPort ePort : thePorts ) {
				System.out.println( "-----> " + ePort.toString() );
			}
		}
	}

	@Test
	public void testSendReceive1() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( BaudRate.BPS_115200 );
				theReceiverPort.open( BaudRate.BPS_115200 );
				final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
				final Thread t = new Thread( () -> {
					try {
						while ( theReceiverPort.isOpened() ) {
							final byte theByte = theReceiverPort.receiveByte();
							theOut.write( theByte );
						}
					}
					catch ( IOException e ) {
						if ( !theReceiverPort.isClosed() ) {
							System.out.println( Trap.asMessage( e ) );
							e.printStackTrace();
						}
					}
				} );
				t.start();
				theTransmitPort.transmitBytes( MESSAGE.getBytes() );
				thePortTestBench.waitForPortCatchUp();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theOut );
				}
				assertEquals( MESSAGE, theOut.toString() );
			}
		}
	}

	@Test
	public void testSendReceive2() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( BaudRate.BPS_115200 );
				theReceiverPort.open( BaudRate.BPS_115200 );
				final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
				final Thread t = new Thread( new Runnable() {
					@Override
					public void run() {
						try {
							final byte[] theByte = theReceiverPort.receiveBytes( MESSAGE.getBytes().length );
							theOut.write( theByte );
						}
						catch ( IOException e ) {
							if ( !theReceiverPort.isClosed() ) {
								System.out.println( Trap.asMessage( e ) );
								e.printStackTrace();
							}
						}
					}
				} );
				t.start();
				theTransmitPort.transmitBytes( MESSAGE.getBytes() );
				thePortTestBench.waitForPortCatchUp();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theOut );
				}
				assertEquals( MESSAGE, theOut.toString() );
			}
		}
	}

	@Test
	public void testReceiveSegment() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPortMetrics theMetrics = new TtyPortMetrics( BaudRate.BPS_115200, 8, StopBits.ONE, Parity.EVEN, Handshake.AUTO, IoTimeout.NORM.getTimeMillis(), -1 );
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( theMetrics );
				theReceiverPort.open( theMetrics );
				final Segment theTransmitSegment = crcPrefixSegment( allocSegment( stringSection( MESSAGE ) ), CrcStandard.CRC_16_CCITT_FALSE );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theTransmitSegment.toSchema() );
				}
				final StringSection theStringSegment;
				final Segment theReceiveSegment = crcPrefixSegment( allocSegment( theStringSegment = stringSection() ), CrcStandard.CRC_16_CCITT_FALSE );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiveSegment );
				theTransmitPort.transmitSegment( theTransmitSegment );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theReceiveSegment.toSchema() );
				}
				assertEquals( MESSAGE, theStringSegment.getPayload() );
				assertArrayEquals( theTransmitSegment.toSequence().toBytes(), theReceiveSegment.toSequence().toBytes() );
			}
		}
	}

	@Test
	public void testOnReceiveSegment() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( BaudRate.BPS_115200 );
				theReceiverPort.open( BaudRate.BPS_115200 );
				final Segment theTransmitSegment = crcPrefixSegment( allocSegment( stringSection( MESSAGE ) ), CrcStandard.CRC_16_CCITT_FALSE );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theTransmitSegment.toSchema() );
				}
				final StringSection theStringSegment;
				final Segment theReceiveSegment = crcPrefixSegment( allocSegment( theStringSegment = stringSection() ), CrcStandard.CRC_16_CCITT_FALSE );
				final boolean _hasReceived[] = { false };
				theReceiverPort.onReceiveSegment( theReceiveSegment, segment -> _hasReceived[0] = true );
				theTransmitPort.transmitSegment( theTransmitSegment );
				final RetryCounter theCounter = new RetryCounter( 5 );
				while ( !_hasReceived[0] && theCounter.hasNextRetry() ) {
					theCounter.nextRetry();
					thePortTestBench.waitLongForPortCatchUp();
				}
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theReceiveSegment.toSchema() );
				}
				assertEquals( MESSAGE, theStringSegment.getPayload() );
				assertArrayEquals( theTransmitSegment.toSequence().toBytes(), theReceiveSegment.toSequence().toBytes() );
			}
		}
	}

	@Test
	public void testTimeout1() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( BaudRate.BPS_115200 );
				theReceiverPort.open( BaudRate.BPS_115200 );
				final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
				final Thread t = new Thread( new Runnable() {
					@Override
					public void run() {
						try {
							while ( theReceiverPort.isOpened() ) {
								final byte theByte = theReceiverPort.receiveByteWithin( 10000 );
								theOut.write( theByte );
							}
						}
						catch ( IOException e ) {
							if ( !theReceiverPort.isClosed() ) {
								System.out.println( Trap.asMessage( e ) );
								e.printStackTrace();
							}
						}
					}
				} );
				t.start();
				theTransmitPort.transmitBytes( MESSAGE.getBytes() );
				thePortTestBench.waitLongForPortCatchUp();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theOut );
				}
				assertEquals( MESSAGE, theOut.toString() );
			}
		}
	}

	@Test
	public void testTimeout2() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( BaudRate.BPS_2400 );
				theReceiverPort.open( BaudRate.BPS_2400 );
				final boolean[] hasException = { false };
				final Thread t = new Thread( new Runnable() {
					@Override
					public void run() {
						while ( theReceiverPort.isOpened() ) {
							try {
								theReceiverPort.receiveByteWithin( 1000 );
							}
							catch ( IOException e ) {
								hasException[0] = true;
							}
						}
					}
				} );
				t.start();
				// Wait longer than the timeout! |-->
				try {
					Thread.sleep( 1500 );
				}
				catch ( InterruptedException ignore ) {}
				// Wait longer than the timeout! <--|
				assertTrue( hasException[0] );
			}
		}
	}

	@Test
	public void testTimeout3() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( BaudRate.BPS_19200 );
				theReceiverPort.open( BaudRate.BPS_19200 );
				final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
				final IOException theException[] = new IOException[1];
				final Thread t = new Thread( new Runnable() {
					@Override
					public void run() {
						try {
							final byte[] theByte = theReceiverPort.receiveBytesWithin( 1000, MESSAGE.getBytes().length );
							theOut.write( theByte );
						}
						catch ( IOException e ) {
							if ( SystemProperty.LOG_TESTS.isEnabled() ) {
								System.out.println( "Caught expected exception: " + e.getMessage() );
							}
							theException[0] = e;
						}
					}
				} );
				t.start();
				theTransmitPort.transmitBytes( MESSAGE.getBytes() );
				thePortTestBench.waitForPortCatchUp();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theOut );
				}
				theReceiverPort.skipAvailable(); // Clean up bits and pieces
				assertNotEquals( MESSAGE, theOut.toString() );
				assertNotNull( theException[0] );

				theTransmitPort.close(); // Required event though if auto close !?!
				theReceiverPort.close(); // Required event though if auto close !?!
			}
		}
	}

	@Disabled("Just for testing edge cases!")
	@Test
	public void testEdgeCase1() throws IOException {
		try ( TtyPortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final TtyPortMetrics theMetrics = new TtyPortMetrics( BaudRate.BPS_115200, 8, StopBits.ONE, Parity.EVEN );
				final TtyPort theTransmitPort = thePortTestBench.getTransmitterPort();
				final TtyPort theReceiverPort = thePortTestBench.getReceiverPort();
				theTransmitPort.open( theMetrics );
				theReceiverPort.open( theMetrics );
				final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
				final Thread t = new Thread( new Runnable() {
					@Override
					public void run() {
						try {
							while ( theReceiverPort.isOpened() ) {
								final byte theByte = theReceiverPort.receiveByte();
								theOut.write( theByte );
							}
						}
						catch ( IOException e ) {
							if ( !theReceiverPort.isClosed() ) {
								System.out.println( Trap.asMessage( e ) );
								e.printStackTrace();
							}
						}
					}
				} );
				t.start();
				final byte[] theTransmitMsg = new byte[] { (byte) -100, 0x35, 0x77, 0x0b };
				theTransmitPort.transmitBytes( theTransmitMsg );
				thePortTestBench.waitForPortCatchUp();
				final byte[] theReceivedMsg = theOut.toByteArray();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Transmit: " + NumericalUtility.toHexString( ":", theTransmitMsg ) );
					System.out.println( "Received: " + NumericalUtility.toHexString( ":", theReceivedMsg ) );
				}
				assertEquals( NumericalUtility.toHexString( ":", theTransmitMsg ), NumericalUtility.toHexString( ":", theReceivedMsg ) );
			}
		}
	}
}
