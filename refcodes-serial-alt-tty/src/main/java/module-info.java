module org.refcodes.serial.alt.tty {
	requires transitive org.refcodes.serial;
	requires com.fazecast.jSerialComm;

	exports org.refcodes.serial.alt.tty;
}
