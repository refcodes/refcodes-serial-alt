// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import org.refcodes.serial.FlowControlType;

/**
 * The {@link Handshake} as of
 * "https://en.wikipedia.org/wiki/Serial_port#Flow_control"
 */
public enum Handshake {

	/**
	 * "If no handshaking is employed, an overrun receiver might simply fail to
	 * receive data from the transmitter."
	 */
	NONE(FlowControlType.NONE),

	/**
	 * "Software handshaking is done for example with ASCII control characters
	 * XON/XOFF to control the flow of data."
	 */
	SOFTWARE(FlowControlType.SOFTWARE),

	/**
	 * "Hardware handshaking is done with extra signals, often the RS-232
	 * RTS/CTS or DTR/DSR signal circuits."
	 */
	RTS(FlowControlType.HARDWARE),

	/**
	 * "Hardware handshaking is done with extra signals, often the RS-232
	 * RTS/CTS or DTR/DSR signal circuits."
	 */
	DTR(FlowControlType.HARDWARE),

	/**
	 * The underlying system's implementation or other implementation specific
	 * handshaking is used.
	 */
	AUTO(FlowControlType.UNDEFINED);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES: 
	// /////////////////////////////////////////////////////////////////////////

	private FlowControlType _flowControlType;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS: 
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link Handshake} enumeration with the acording
	 * {@link FlowControlType}.
	 * 
	 * @param aFlowControlType The type of flow control.
	 */
	private Handshake( FlowControlType aFlowControlType ) {
		_flowControlType = aFlowControlType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS: 
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether the {@link Handshake} represents a hardware flow
	 * control.
	 * 
	 * @return True in case of hardware flow control.
	 */
	public boolean isHardwareFlowControl() {
		return _flowControlType == FlowControlType.HARDWARE;
	}

	/**
	 * Determines whether the {@link Handshake} represents a software flow
	 * control.
	 * 
	 * @return True in case of software flow control.
	 */
	public boolean isSoftwareFlowControl() {
		return _flowControlType == FlowControlType.SOFTWARE;
	}

	/**
	 * Retrieves the {@link FlowControlType}.
	 * 
	 * @return The according flow control type (hardware or software) .
	 */
	public FlowControlType getFlowControlType() {
		return _flowControlType;
	}
}
