// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.exception.Trap;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.LoopbackPort;
import org.refcodes.serial.PortMetrics;
import org.refcodes.serial.PortTestBench;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * The {@link TtyPortTestBench} implements the {@link PortTestBench} for the
 * {@link LoopbackPort} type.
 */
public class TtyPortTestBench implements PortTestBench {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( TtyPortTestBench.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[] PORT_TYPES = { "ttl232", "ftdi_sio", "ft232" }; // , "physical" };

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String[] _portTypes;
	private TtyPortMetrics _portMetrics;
	private TtyPort _port1 = null;
	private TtyPort _port2 = null;
	private String _alias1 = null;
	private String _alias2 = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the according {@link TtyPortTestBench} instance.
	 */
	public TtyPortTestBench() {
		this( null, PORT_TYPES );
	}

	/**
	 * Constructs the according {@link TtyPortTestBench} instance.
	 *
	 * @param aPortTypes A list of strings of which one must be contained in the
	 *        according ports' aliases (names) of the according port pair in
	 *        question to be created (the strings are probed in the order as
	 *        provided).
	 */
	public TtyPortTestBench( String... aPortTypes ) {
		this( null, aPortTypes );
	}

	/**
	 * Constructs the according {@link TtyPortTestBench} instance.
	 *
	 * @param aPortMetrics The {@link PortMetrics} to use when opening the ports
	 *        via {@link #open()}.
	 */
	public TtyPortTestBench( TtyPortMetrics aPortMetrics ) {
		this( aPortMetrics, PORT_TYPES );
	}

	/**
	 * Constructs the according {@link TtyPortTestBench} instance.
	 * 
	 * @param aPortMetrics The {@link PortMetrics} to use when opening the ports
	 *        via {@link #open()}.
	 * 
	 * @param aPortTypes A list of strings of which one must be contained in the
	 *        according ports' aliases (names) of the according port pair in
	 *        question to be created (the strings are probed in the order as
	 *        provided).
	 */
	public TtyPortTestBench( TtyPortMetrics aPortMetrics, String... aPortTypes ) {
		aPortTypes = aPortTypes != null && aPortTypes.length != 0 ? aPortTypes : PORT_TYPES;
		_portTypes = aPortTypes;
		_portMetrics = aPortMetrics;
		try {
			int i;
			final TtyPortHub thePortHub = new TtyPortHub();
			out: {
				for ( String eCandidate : aPortTypes ) {
					i = 0;
					for ( TtyPort ePort : thePortHub.ports() ) {
						if ( ePort.getName().toLowerCase().contains( eCandidate ) || ePort.getDescription().toLowerCase().contains( eCandidate ) ) {
							if ( i == 0 ) {
								if ( SystemProperty.LOG_TESTS.isEnabled() ) {
									LOGGER.log( Level.FINEST, "Found 1st port <" + ePort + "> to use ..." );
								}
								_alias1 = ePort.getAlias();
							}
							if ( i == 1 ) {
								if ( SystemProperty.LOG_TESTS.isEnabled() ) {
									LOGGER.log( Level.FINEST, "Found 2nd port <" + ePort + "> to use ..." );
								}
								_alias2 = ePort.getAlias();
								break out;
							}
							i++;
						}
					}
				}
			}
		}
		catch ( IOException e ) {
			LOGGER.log( Level.WARNING, "Failed to access any TTY ports: " + Trap.asMessage( e ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPorts() {
		boolean hasPorts = _alias1 != null && _alias2 != null;
		if ( !hasPorts ) {
			LOGGER.log( Level.WARNING, "Failed to determine a pair of ports with types < " + VerboseTextBuilder.asString( _portTypes ) + "> (please connect your null modem cable to two serial ports on your box)!" );
		}
		return hasPorts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort getReceiverPort() throws IOException {
		if ( _port1 == null ) {
			synchronized ( this ) {
				if ( _port1 == null ) {
					_port1 = new TtyPortHub().toPort( _alias1, _portMetrics );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						LOGGER.log( Level.INFO, "Using 1st port <" + _port1 + "> ..." );
					}
				}
			}
		}
		return _port1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort getTransmitterPort() throws IOException {
		if ( _port2 == null ) {
			synchronized ( this ) {
				if ( _port2 == null ) {
					_port2 = new TtyPortHub().toPort( _alias2, _portMetrics );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						LOGGER.log( Level.INFO, "Using 2nd port <" + _port2 + "> ..." );
					}
				}
			}
		}
		return _port2;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		if ( _portMetrics != null ) {
			getReceiverPort().open( _portMetrics );
			getTransmitterPort().open( _portMetrics );
		}
		else {
			getReceiverPort().open();
			getTransmitterPort().open();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		try {
			if ( _port1 != null ) {
				_port1.close();
			}
		}
		finally {
			if ( _port2 != null ) {
				_port2.close();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitShortestForPortCatchUp() {
		try {
			Thread.sleep( 100 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitShortForPortCatchUp() {
		try {
			Thread.sleep( 400 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForPortCatchUp() {
		try {
			Thread.sleep( 500 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitLongForPortCatchUp() {
		try {
			Thread.sleep( 600 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitLongestForPortCatchUp() {
		try {
			Thread.sleep( 1000 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForPortCatchUp( long aSleepTimeMillis ) {
		try {
			Thread.sleep( aSleepTimeMillis );
		}
		catch ( InterruptedException ignore ) {}
	}
}
