// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

/**
 * Defines the number of valid TTY stop bits.
 */
public enum StopBits {

	/**
	 * The underlying system's implementation or other implementation specific
	 * stop bit settings are used.
	 */
	AUTO(-1F),

	/**
	 * One stop bit is used.
	 */
	ONE(1F),

	/**
	 * One and a half stop bits are used.
	 */
	ONE_POINT_FIVE(1.5F),

	/**
	 * Two stop bits are used.
	 */
	TWO(2F);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private float _stopBits;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private StopBits( Float aStopBits ) {
		_stopBits = aStopBits;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the number of stop bits.
	 * 
	 * @return The number of stop bits.
	 */
	float getStopBits() {
		return _stopBits;
	}
}
