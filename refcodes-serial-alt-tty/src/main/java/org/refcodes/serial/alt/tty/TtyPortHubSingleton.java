// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.serial.alt.tty;

/**
 * The singleton of the {@link TtyPortHub} for system wide COM port management.
 */
public class TtyPortHubSingleton extends TtyPortHub {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static TtyPortHubSingleton _singleton = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new port manager singleton.
	 */
	protected TtyPortHubSingleton() {}

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link TtyPortHubSingleton}.
	 * 
	 * @return The {@link TtyPortHub} singleton's instance.
	 */
	public static TtyPortHub getInstance() {
		if ( _singleton == null ) {
			synchronized ( TtyPortHubSingleton.class ) {
				if ( _singleton == null ) {
					_singleton = new TtyPortHubSingleton();
				}
			}
		}
		return _singleton;
	}
}
