// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.serial.NoSuchPortExcpetion;
import org.refcodes.serial.PortHub;

import com.fazecast.jSerialComm.SerialPort;

/**
 * The {@link TtyPortHub} implements the {@link PortHub} for TTY-/COM-Port
 * hardware.
 */
public class TtyPortHub implements PortHub<TtyPort, TtyPortMetrics> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExecutorService _executorService;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link TtyPortHub}.
	 */
	public TtyPortHub() {
		this( ControlFlowUtility.createCachedExecutorService( true ) );
	}

	/**
	 * Creates a {@link TtyPortHub} with the given {@link ExecutorService} to be
	 * used by asynchronous functionality.
	 * 
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        invoking asynchronously working methods.
	 */
	public TtyPortHub( ExecutorService aExecutorService ) {
		_executorService = aExecutorService != null ? aExecutorService : ControlFlowUtility.createCachedExecutorService( true );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort[] ports() throws IOException {
		final SerialPort[] theSerialPorts = SerialPort.getCommPorts();
		if ( theSerialPorts != null ) {
			final TtyPort[] theTtyPorts = new TtyPort[theSerialPorts.length];
			for ( int i = 0; i < theSerialPorts.length; i++ ) {
				theTtyPorts[i] = toTtyPort( theSerialPorts[i], new TtyPortMetrics( theSerialPorts[i] ) );
			}
			Arrays.sort( theTtyPorts );
			return theTtyPorts;
		}
		return new TtyPort[0];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort toPort( String aAlias, TtyPortMetrics aPortMetrics ) throws IOException {
		SerialPort theSerialPort = SerialPort.getCommPort( aAlias );
		if ( theSerialPort == null ) {
			out: {
				final SerialPort[] theSerialPorts = SerialPort.getCommPorts();
				if ( theSerialPorts != null ) {
					for ( int i = 0; i < theSerialPorts.length; i++ ) {
						if ( aAlias.equalsIgnoreCase( theSerialPorts[i].getSystemPortName() ) ) {
							theSerialPort = theSerialPorts[i];
							break out;
						}
					}
					throw new NoSuchPortExcpetion( aAlias, "No such port with the given alias <" + aAlias + "> found!" );
				}
			}
		}
		return toTtyPort( theSerialPort, aPortMetrics != null ? aPortMetrics : new TtyPortMetrics( theSerialPort ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private TtyPort toTtyPort( SerialPort aSerialPort, TtyPortMetrics aPortMetrics ) {
		return new TtyPort( aSerialPort.getSystemPortName(), aPortMetrics != null ? aPortMetrics : new TtyPortMetrics( aSerialPort ), aSerialPort.getDescriptivePortName(), aSerialPort.getPortDescription(), aSerialPort, _executorService );
	}
}
