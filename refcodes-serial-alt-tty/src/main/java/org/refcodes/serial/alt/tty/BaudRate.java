// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

/**
 * Defines some commonly used bits/second rates (BPS != Baud, but Baud often is
 * used synonymously for BPS and gives a much better class name than BPS).
 */
public enum BaudRate {

	BPS_75(75),

	BPS_110(110),

	BPS_150(150),

	BPS_300(300),

	BPS_600(600),

	BPS_1200(1200),

	BPS_1800(1800),

	BPS_2400(2400),

	BPS_4800(4800),

	BPS_7200(7200),

	BPS_9600(9600),

	BPS_14400(14400),

	BPS_19200(19200),

	BPS_38400(38400),

	BPS_56000(56000),

	BPS_57600(57600),

	BPS_115200(115200),

	BPS_128000(128000),

	BPS_256000(256000),

	BPS_960000_FTDI(960000),

	BPS_1000000_FTDI(1000000),

	BPS_1200000_FTDI(1200000),

	BPS_1500000_FTDI(1500000),

	BPS_2000000_FTDI(2000000),

	BPS_3000000_FTDI(3000000);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _bitsPerSecond;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private BaudRate( int aBitsPerSecond ) {
		_bitsPerSecond = aBitsPerSecond;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the bits per second.
	 * 
	 * @return The bits/second.
	 */
	public int getBitsPerSecond() {
		return _bitsPerSecond;
	}
}
