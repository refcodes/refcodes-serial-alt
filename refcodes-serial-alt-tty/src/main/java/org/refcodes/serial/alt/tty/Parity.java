// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

/**
 * The {@link Parity} as of "https://en.wikipedia.org/wiki/Serial_port#Parity"
 */
public enum Parity {

	/**
	 * The underlying system's implementation or other implementation specific
	 * parity is used.
	 */
	AUTO,

	/**
	 * "None (N) means that no parity bit is sent at all."
	 */
	NONE,

	/**
	 * "Odd (O) means that parity bit is set so that the number of "logical
	 * ones" must be odd."
	 */
	ODD,

	/**
	 * "Even (E) means that parity bit is set so that the number of "logical
	 * ones" must be even."
	 */
	EVEN,

	/**
	 * "Mark (M) parity means that the parity bit is always set to the mark
	 * signal condition (logical 1)."
	 */
	MARK,

	/**
	 * "Space (S) parity always sends the parity bit in the space signal
	 * condition (logical 0)."
	 */
	SPACE
}