// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import org.refcodes.mixin.ReadTimeoutMillisAccessor;
import org.refcodes.mixin.WriteTimeoutMillisAccessor;
import org.refcodes.numerical.BitwiseOperationBuilder;
import org.refcodes.serial.Port;
import org.refcodes.serial.PortMetrics;

import com.fazecast.jSerialComm.SerialPort;

/**
 * The {@link TtyPortMetrics} describe a COM or TTY serial {@link Port} on your
 * computer.
 */
public class TtyPortMetrics implements PortMetrics, ReadTimeoutMillisAccessor, WriteTimeoutMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final int DEFAULT_DATA_BITS = Byte.SIZE; // As we work with bytes all over here, we use 8 data bits by default!
	private static final int DEFAULT_BAUD_RATE = BaudRate.BPS_9600.getBitsPerSecond();
	private static final Parity DEFAULT_PARTITY = Parity.AUTO;
	private static final StopBits DEFAULT_STOP_BITS = StopBits.AUTO;
	private static final Handshake DEFAULT_HANDSHAKE = Handshake.AUTO;
	private static final long DEFAULT_READ_TIMEOUT_IN_MS = -1;
	private static final long DEFAULT_WRITE_TIMEOUT_IN_MS = -1;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _baudRate = DEFAULT_BAUD_RATE;
	private Parity _parity = DEFAULT_PARTITY;
	private int _dataBits = DEFAULT_DATA_BITS;
	private StopBits _stopBits = DEFAULT_STOP_BITS;
	private Handshake _handshake = DEFAULT_HANDSHAKE;
	private long _readTimeoutInMs = DEFAULT_READ_TIMEOUT_IN_MS;
	private long _writeTimeoutInMs = DEFAULT_WRITE_TIMEOUT_IN_MS;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private TtyPortMetrics( Builder aBuilder ) {
		this._baudRate = aBuilder.baudRate;
		this._parity = aBuilder.parity;
		this._dataBits = aBuilder.dataBits;
		this._stopBits = aBuilder.stopBits;
		this._handshake = aBuilder.handshake;
		this._readTimeoutInMs = aBuilder.readTimeoutInMs;
		this._writeTimeoutInMs = aBuilder.writeTimeoutInMs;
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs the {@link PortMetrics} instance.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * @param aReadTimeoutInMs The read timeout to be set.
	 * @param aWriteTimeoutInMs The write timeout to be set.
	 */
	public TtyPortMetrics( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake, long aReadTimeoutInMs, long aWriteTimeoutInMs ) {
		_baudRate = aBaudRate;
		_dataBits = aDataBits;
		_stopBits = aStopBits;
		_parity = aParity;
		_handshake = aHandshake;
		_readTimeoutInMs = aReadTimeoutInMs;
		_writeTimeoutInMs = aWriteTimeoutInMs;
	}

	/**
	 * Constructs the {@link PortMetrics} instance.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * @param aReadTimeoutInMs The read timeout to be set.
	 * @param aWriteTimeoutInMs The write timeout to be set.
	 */
	public TtyPortMetrics( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake, long aReadTimeoutInMs, long aWriteTimeoutInMs ) {
		_baudRate = aBaudRate.getBitsPerSecond();
		_dataBits = aDataBits;
		_stopBits = aStopBits;
		_parity = aParity;
		_handshake = aHandshake;
		_readTimeoutInMs = aReadTimeoutInMs;
		_writeTimeoutInMs = aWriteTimeoutInMs;
	}

	/**
	 * Constructs the {@link PortMetrics} instance. Timeouts are disabled.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 */
	public TtyPortMetrics( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake ) {
		_baudRate = aBaudRate;
		_dataBits = aDataBits;
		_stopBits = aStopBits;
		_parity = aParity;
		_handshake = aHandshake;
		_readTimeoutInMs = -1;
		_writeTimeoutInMs = -1;
	}

	/**
	 * Constructs the {@link PortMetrics} instance. Timeouts are disabled.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 */
	public TtyPortMetrics( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake ) {
		_baudRate = aBaudRate.getBitsPerSecond();
		_dataBits = aDataBits;
		_stopBits = aStopBits;
		_parity = aParity;
		_handshake = aHandshake;
		_readTimeoutInMs = -1;
		_writeTimeoutInMs = -1;
	}

	/**
	 * Constructs the {@link PortMetrics} instance. Timeouts are disabled. The
	 * underlying system's implementation or other implementation specific
	 * handshaking is used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 */
	public TtyPortMetrics( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity ) {
		_baudRate = aBaudRate;
		_dataBits = aDataBits;
		_stopBits = aStopBits;
		_parity = aParity;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = -1;
		_writeTimeoutInMs = -1;
	}

	/**
	 * Constructs the {@link PortMetrics} instance. Timeouts are disabled. The
	 * underlying system's implementation or other implementation specific
	 * handshaking is used ({@link Handshake#AUTO}).
	 *
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 */
	public TtyPortMetrics( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity ) {
		_baudRate = aBaudRate.getBitsPerSecond();
		_dataBits = aDataBits;
		_stopBits = aStopBits;
		_parity = aParity;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = -1;
		_writeTimeoutInMs = -1;
	}

	/**
	 * Constructs the {@link PortMetrics} instance. Timeouts are disabled. The
	 * underlying system's implementation or other implementation specific
	 * handshaking, stop bits and parity are used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The baud rate to be set.
	 */
	public TtyPortMetrics( int aBaudRate ) {
		_baudRate = aBaudRate;
		_dataBits = DEFAULT_DATA_BITS;
		_stopBits = StopBits.AUTO;
		_parity = Parity.AUTO;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = -1;
		_writeTimeoutInMs = -1;
	}

	/**
	 * Constructs the {@link PortMetrics} instance. Timeouts are disabled. The
	 * underlying system's implementation or other implementation specific
	 * handshaking, stop bits and parity are used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 */
	public TtyPortMetrics( BaudRate aBaudRate ) {
		_baudRate = aBaudRate.getBitsPerSecond();
		_dataBits = DEFAULT_DATA_BITS;
		_stopBits = StopBits.AUTO;
		_parity = Parity.AUTO;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = -1;
		_writeTimeoutInMs = -1;
	}

	/**
	 * Constructs the {@link PortMetrics} instance.
	 *
	 * @param aReadTimeoutInMs The read timeout to be set.
	 * @param aWriteTimeoutInMs The write timeout to be set.
	 */
	public TtyPortMetrics( long aReadTimeoutInMs, long aWriteTimeoutInMs ) {
		_baudRate = DEFAULT_BAUD_RATE;
		_dataBits = DEFAULT_DATA_BITS;
		_stopBits = StopBits.AUTO;
		_parity = Parity.AUTO;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = aReadTimeoutInMs;
		_writeTimeoutInMs = aWriteTimeoutInMs;
	}

	/**
	 * Constructs the {@link PortMetrics} instance.
	 *
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aReadTimeoutInMs The read timeout to be set.
	 * @param aWriteTimeoutInMs The write timeout to be set.
	 */
	public TtyPortMetrics( BaudRate aBaudRate, long aReadTimeoutInMs, long aWriteTimeoutInMs ) {
		_baudRate = aBaudRate.getBitsPerSecond();
		_dataBits = DEFAULT_DATA_BITS;
		_stopBits = StopBits.AUTO;
		_parity = Parity.AUTO;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = aReadTimeoutInMs;
		_writeTimeoutInMs = aWriteTimeoutInMs;
	}

	/**
	 * Constructs the {@link PortMetrics} instance.
	 *
	 * @param aBaudRate The baud rate to be set.
	 * @param aReadTimeoutInMs The read timeout to be set.
	 * @param aWriteTimeoutInMs The write timeout to be set.
	 */
	public TtyPortMetrics( int aBaudRate, long aReadTimeoutInMs, long aWriteTimeoutInMs ) {
		_baudRate = aBaudRate;
		_dataBits = DEFAULT_DATA_BITS;
		_stopBits = StopBits.AUTO;
		_parity = Parity.AUTO;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = aReadTimeoutInMs;
		_writeTimeoutInMs = aWriteTimeoutInMs;
	}

	/**
	 * Constructs the {@link PortMetrics} instance. Timeouts are disabled. The
	 * underlying system's implementation or other implementation specific baud
	 * rate, handshaking, stop bits and parity are used
	 * ({@link Handshake#AUTO}).
	 */
	public TtyPortMetrics() {
		_baudRate = -1;
		_dataBits = DEFAULT_DATA_BITS;
		_stopBits = StopBits.AUTO;
		_parity = Parity.AUTO;
		_handshake = Handshake.AUTO;
		_readTimeoutInMs = -1;
		_writeTimeoutInMs = -1;
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs the {@link TtyPortMetrics} from the underlying
	 * {@link SerialPort} instance.
	 * 
	 * @param aSerialPort The {@link SerialPort} from which to construct this
	 *        instance.
	 */
	TtyPortMetrics( SerialPort aSerialPort ) {
		_baudRate = aSerialPort.getBaudRate();

		_stopBits = switch ( aSerialPort.getNumStopBits() ) {
		case SerialPort.ONE_STOP_BIT -> StopBits.ONE;
		case SerialPort.ONE_POINT_FIVE_STOP_BITS -> StopBits.ONE_POINT_FIVE;
		case SerialPort.TWO_STOP_BITS -> StopBits.TWO;
		default -> StopBits.AUTO;
		};

		final int theFlowControl = aSerialPort.getFlowControlSettings();
		if ( BitwiseOperationBuilder.isMaskable( theFlowControl, SerialPort.FLOW_CONTROL_RTS_ENABLED ) || BitwiseOperationBuilder.isMaskable( theFlowControl, SerialPort.FLOW_CONTROL_CTS_ENABLED ) ) {
			_handshake = Handshake.RTS;
		}
		else if ( BitwiseOperationBuilder.isMaskable( theFlowControl, SerialPort.FLOW_CONTROL_DTR_ENABLED ) || BitwiseOperationBuilder.isMaskable( theFlowControl, SerialPort.FLOW_CONTROL_DSR_ENABLED ) ) {
			_handshake = Handshake.DTR;
		}
		else if ( BitwiseOperationBuilder.isMaskable( theFlowControl, SerialPort.FLOW_CONTROL_XONXOFF_IN_ENABLED ) || BitwiseOperationBuilder.isMaskable( theFlowControl, SerialPort.FLOW_CONTROL_XONXOFF_OUT_ENABLED ) ) {
			_handshake = Handshake.SOFTWARE;
		}
		else {
			_handshake = Handshake.NONE;
		}

		_dataBits = aSerialPort.getNumDataBits();

		final int thePartiy = aSerialPort.getParity();
		_parity = switch ( thePartiy ) {
		case SerialPort.EVEN_PARITY -> Parity.EVEN;
		case SerialPort.MARK_PARITY -> Parity.MARK;
		case SerialPort.NO_PARITY -> Parity.NONE;
		case SerialPort.ODD_PARITY -> Parity.ODD;
		case SerialPort.SPACE_PARITY -> Parity.SPACE;
		default -> Parity.AUTO;
		};

		_readTimeoutInMs = aSerialPort.getReadTimeout() == 0 ? -1 : aSerialPort.getReadTimeout();
		_writeTimeoutInMs = aSerialPort.getWriteTimeout() == 0 ? -1 : aSerialPort.getWriteTimeout();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The baud rate of the port.
	 * 
	 * @return The port's baud rate.
	 */
	public int getBaudRate() {
		return _baudRate;
	}

	/**
	 * The {@link Parity} of the port.
	 * 
	 * @return The port's parity.
	 */
	public Parity getParity() {
		return _parity;
	}

	/**
	 * The number of data bits used by the port.
	 * 
	 * @return The data bits used by the port.
	 */
	public int getDataBits() {
		return _dataBits;
	}

	/**
	 * The number of stop bits used by the port.
	 * 
	 * @return The stop bits used by the port.
	 */
	public StopBits getStopBits() {
		return _stopBits;
	}

	/**
	 * The kind of {@link Handshake} used by the port.
	 * 
	 * @return The port's handshake being used.
	 */
	public Handshake getHandshake() {
		return _handshake;
	}

	/**
	 * The read timeout in milliseconds.
	 * 
	 * @return The read timeout.
	 */
	@Override
	public long getReadTimeoutMillis() {
		return _readTimeoutInMs;
	}

	/**
	 * The write timeout in milliseconds.
	 * 
	 * @return The write timeout.
	 */
	@Override
	public long getWriteTimeoutMillis() {
		return _writeTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [baudRate=" + _baudRate + ", parity=" + _parity + ", dataBits=" + _dataBits + ", stopBits=" + _stopBits + ", handshake=" + _handshake + ", readTimeoutInMs=" + _readTimeoutInMs + ", writeTimeoutInMs=" + _writeTimeoutInMs + "]";
	}

	/**
	 * Creates builder to build {@link TtyPortMetrics}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link TtyPortMetrics} instances.
	 */
	public static final class Builder implements ReadTimeoutMillisBuilder<Builder>, WriteTimeoutMillisBuilder<Builder> {

		protected int baudRate = DEFAULT_BAUD_RATE;
		protected Parity parity = DEFAULT_PARTITY;
		protected int dataBits = DEFAULT_DATA_BITS;
		protected StopBits stopBits = DEFAULT_STOP_BITS;
		protected Handshake handshake = DEFAULT_HANDSHAKE;
		protected long readTimeoutInMs = DEFAULT_READ_TIMEOUT_IN_MS;
		protected long writeTimeoutInMs = DEFAULT_WRITE_TIMEOUT_IN_MS;

		private Builder() {}

		/**
		 * The baud rate of the port.
		 * 
		 * @param aBaudRate The port's baud rate.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withBaudRate( int aBaudRate ) {
			baudRate = aBaudRate;
			return this;
		}

		/**
		 * The baud rate of the port.
		 * 
		 * @param aBaudRate The port's baud rate.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withBaudRate( BaudRate aBaudRate ) {
			baudRate = aBaudRate.getBitsPerSecond();
			return this;
		}

		/**
		 * The {@link Parity} of the port.
		 * 
		 * @param aParity The port's parity.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withParity( Parity aParity ) {
			parity = aParity;
			return this;
		}

		/**
		 * The number of data bits used by the port.
		 * 
		 * @param aDataBits The data bits used by the port.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withDataBits( int aDataBits ) {
			dataBits = aDataBits;
			return this;
		}

		/**
		 * The number of stop bits used by the port.
		 * 
		 * @param aStopBits The stop bits used by the port.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withStopBits( StopBits aStopBits ) {
			stopBits = aStopBits;
			return this;
		}

		/**
		 * The kind of {@link Handshake} used by the port.
		 * 
		 * @param aHandshake The port's handshake being used.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withHandshake( Handshake aHandshake ) {
			handshake = aHandshake;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadTimeoutMillis( long aReadTimeoutInMs ) {
			readTimeoutInMs = aReadTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withWriteTimeoutMillis( long aWriteTimeoutInMs ) {
			writeTimeoutInMs = aWriteTimeoutInMs;
			return this;
		}

		/**
		 * Builds the according {@link TtyPortMetrics} from this
		 * {@link Builder}.
		 * 
		 * @return The accordingly configured {@link TtyPortMetrics}.
		 */
		public TtyPortMetrics build() {
			return new TtyPortMetrics( this );
		}
	}
}
