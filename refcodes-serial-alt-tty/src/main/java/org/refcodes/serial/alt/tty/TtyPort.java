// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.alt.tty;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.exception.BugException;
import org.refcodes.io.TimeoutInputStream;
import org.refcodes.mixin.DescriptionAccessor;
import org.refcodes.mixin.NameAccessor;
import org.refcodes.serial.AbstractPort;
import org.refcodes.serial.Port;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.SerialUtility;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

/**
 * A {@link TtyPort} is a {@link Port} implementation harnessing the serial
 * (COM) port.
 */
public class TtyPort extends AbstractPort<TtyPortMetrics> implements NameAccessor, DescriptionAccessor, AutoCloseable {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final Parity DEFAULT_PARITY = Parity.EVEN;
	public static final StopBits DEFAULT_STOP_BITS = StopBits.ONE_POINT_FIVE;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _name;
	private String _description;
	private SerialPort _serialPort;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link Port} with the given properties. The alias
	 * corresponds to the underlying TTY-/COM-Port's name.
	 * 
	 * @param aAlias The unambiguous technical name for this {@link Port}.
	 * @param aPortMetrics The metrics of the {@link Port}.
	 * @param aName The human readable (not necessarily technical or
	 *        unambiguous) name for this {@link Port}.
	 * @param aDescription The human readable description about the port.
	 * @param aSerialPort The underlying {@link SerialPort} being used for this
	 *        implementation.
	 */
	protected TtyPort( String aAlias, TtyPortMetrics aPortMetrics, String aName, String aDescription, SerialPort aSerialPort ) {
		this( aAlias, aPortMetrics, aName, aDescription, aSerialPort, ControlFlowUtility.createCachedExecutorService( true ) );
	}

	/**
	 * Constructs a {@link Port} with the given properties and the given
	 * {@link ExecutorService} to be used by asynchronous functionality. The
	 * alias corresponds to the underlying TTY-/COM-Port's name.
	 * 
	 * @param aAlias The unambiguous technical name for this {@link Port}.
	 * @param aPortMetrics The metrics of the {@link Port}.
	 * @param aName The human readable (not necessarily technical or
	 *        unambiguous) name for this {@link Port}.
	 * @param aDescription The human readable description about the port.
	 * @param aSerialPort The underlying {@link SerialPort} being used for this
	 *        implementation.
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        invoking asynchronously working methods.
	 */
	protected TtyPort( String aAlias, TtyPortMetrics aPortMetrics, String aName, String aDescription, SerialPort aSerialPort, ExecutorService aExecutorService ) {
		super( aAlias, aPortMetrics, aExecutorService );
		_name = aName;
		_description = aDescription;
		_serialPort = aSerialPort;
		_serialPort.flushIOBuffers();
		_serialPort.addDataListener( new SerialPortDataListener() {

			@Override
			public void serialEvent( SerialPortEvent event ) {
				synchronized ( TtyPort.this ) {
					notifyAll(); // Notify any waiting threads upon data available
				}
			}

			@Override
			public int getListeningEvents() {
				return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
			}
		} );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the unambiguous technical name of the TTY-/COM-Port.
	 * 
	 * @return The port's name.
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * Returns the a more (not necessarily technical or unambiguous) name of the
	 * TTY-/COM-Port.
	 * 
	 * @return The port's verbose name.
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription() {
		return _description;
	}

	/**
	 * Determines the size of the read buffer for this {@link TtyPort}.
	 * Depending on the underlying {@link TtyPort} implementation, when not
	 * reading continuously from this port, buffered data may be overwritten
	 * (lost) by succeedingly received data.
	 * 
	 * @return The according buffer size.
	 */
	public int getReadBufferSize() {
		return _serialPort.getDeviceReadBufferSize();
	}

	/**
	 * Determines the size of the write buffer for this {@link TtyPort}.
	 * 
	 * @return The according buffer size.
	 */
	public int getWriteBufferSize() {
		return _serialPort.getDeviceWriteBufferSize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable() {
		return !isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		open( _portMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( TtyPortMetrics aConnection ) throws IOException {
		if ( aConnection != null ) {
			_portMetrics = aConnection;
		}
		super.open( _portMetrics );

		synchronized ( this ) {
			notifyAll();
		}
		synchronized ( _serialPort ) {
			_serialPort.notifyAll();
		}

		if ( !_serialPort.openPort() && !_serialPort.isOpen() ) {
			throw new IOException( "Cannot open port  <" + _serialPort.getSystemPortName() + ">." );
		}

		Parity theParity = _portMetrics.getParity();
		if ( theParity == Parity.AUTO ) {
			theParity = DEFAULT_PARITY;
		}
		final int theSerialPartiy;
		switch ( theParity ) {
		case EVEN:
			theSerialPartiy = SerialPort.EVEN_PARITY;
			break;
		case MARK:
			theSerialPartiy = SerialPort.MARK_PARITY;
			break;
		case NONE:
			theSerialPartiy = SerialPort.NO_PARITY;
			break;
		case ODD:
			theSerialPartiy = SerialPort.ODD_PARITY;
			break;
		case SPACE:
			theSerialPartiy = SerialPort.SPACE_PARITY;
			break;
		case AUTO:
			throw new BugException( "We should not end up in enumeration <" + theParity + ">." );
		default:
			throw new BugException( "The enumeration <" + theParity + "> has been forgotten to get implemented." );
		}
		StopBits theStopBits = _portMetrics.getStopBits();
		if ( theStopBits == StopBits.AUTO ) {
			theStopBits = DEFAULT_STOP_BITS;
		}
		final int theSerialStopBits;
		switch ( theStopBits ) {
		case ONE:
			theSerialStopBits = SerialPort.ONE_STOP_BIT;
			break;
		case ONE_POINT_FIVE:
			theSerialStopBits = SerialPort.ONE_POINT_FIVE_STOP_BITS;
			break;
		case TWO:
			theSerialStopBits = SerialPort.TWO_STOP_BITS;
			break;
		case AUTO:
			throw new BugException( "We should not end up in enumeration <" + theStopBits + ">." );
		default:
			throw new BugException( "The enumeration <" + theStopBits + "> has been forgotten to get implemented." );
		}

		// TIMEOUT_WRITE_BLOCKING slows down enormously! |-->
		// _serialPort.setComPortTimeouts( SerialPort.TIMEOUT_READ_BLOCKING | SerialPort.TIMEOUT_WRITE_BLOCKING, aConnection.getReadTimeoutInMs() > 0 ? (int) aConnection.getReadTimeoutInMs() : _serialPort.getReadTimeout(), aConnection.getWriteTimeoutInMs() > 0 ? (int) aConnection.getWriteTimeoutInMs() : _serialPort.getWriteTimeout() );
		// TIMEOUT_WRITE_BLOCKING slows down enormously! <--|

		_serialPort.setComPortTimeouts( SerialPort.TIMEOUT_READ_BLOCKING, _portMetrics.getReadTimeoutMillis() > 0 ? (int) _portMetrics.getReadTimeoutMillis() : _serialPort.getReadTimeout(), _portMetrics.getWriteTimeoutMillis() > 0 ? (int) _portMetrics.getWriteTimeoutMillis() : _serialPort.getWriteTimeout() );
		_serialPort.setComPortParameters( _portMetrics.getBaudRate(), _portMetrics.getDataBits(), theSerialStopBits, theSerialPartiy );
		// Since JSerialComm 2.10.0 we need some IO init time |-->
		//	try {
		//		Thread.sleep( IoSleepLoopTime.NORM.getTimeMillis() );
		//	}
		//	catch ( InterruptedException ignore ) {}
		// Since JSerialComm 2.10.0 we need some IO init time <--|
	}

	/**
	 * Opens the {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, aDataBits, aStopBits, aParity, aHandshake, aReadTimeout, aWriteTimeout ) );
	}

	/**
	 * Opens the {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, aDataBits, aStopBits, aParity, aHandshake, aReadTimeout, aWriteTimeout ) );
	}

	/**
	 * Opens the {@link TtyPort} instance. Timeouts are disabled.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, aDataBits, aStopBits, aParity, aHandshake, _portMetrics.getReadTimeoutMillis(), _portMetrics.getWriteTimeoutMillis() ) );
	}

	/**
	 * Opens the {@link TtyPort} instance. Timeouts are disabled.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, aDataBits, aStopBits, aParity, aHandshake, _portMetrics.getReadTimeoutMillis(), _portMetrics.getWriteTimeoutMillis() ) );
	}

	/**
	 * Opens the {@link TtyPort} instance. Timeouts are disabled. The underlying
	 * system's implementation or other implementation specific handshaking is
	 * used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, aDataBits, aStopBits, aParity, _portMetrics.getHandshake(), _portMetrics.getReadTimeoutMillis(), _portMetrics.getWriteTimeoutMillis() ) );
	}

	/**
	 * Opens the {@link TtyPort} instance. Timeouts are disabled. The underlying
	 * system's implementation or other implementation specific handshaking is
	 * used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, aDataBits, aStopBits, aParity, _portMetrics.getHandshake(), _portMetrics.getReadTimeoutMillis(), _portMetrics.getWriteTimeoutMillis() ) );
	}

	/**
	 * Opens the {@link TtyPort} instance. Timeouts are disabled. The underlying
	 * system's implementation or other implementation specific handshaking,
	 * stop bits and parity are used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( int aBaudRate ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, _portMetrics.getDataBits(), _portMetrics.getStopBits(), _portMetrics.getParity(), _portMetrics.getHandshake(), _portMetrics.getReadTimeoutMillis(), _portMetrics.getWriteTimeoutMillis() ) );
	}

	/**
	 * Opens the {@link TtyPort} instance. Timeouts are disabled. The underlying
	 * system's implementation or other implementation specific handshaking,
	 * stop bits and parity are used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( BaudRate aBaudRate ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, _portMetrics.getDataBits(), _portMetrics.getStopBits(), _portMetrics.getParity(), _portMetrics.getHandshake(), _portMetrics.getReadTimeoutMillis(), _portMetrics.getWriteTimeoutMillis() ) );
	}

	/**
	 * Opens the {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( BaudRate aBaudRate, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, _portMetrics.getDataBits(), _portMetrics.getStopBits(), _portMetrics.getParity(), _portMetrics.getHandshake(), aReadTimeout, aWriteTimeout ) );
	}

	/**
	 * Opens the {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( int aBaudRate, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( new TtyPortMetrics( aBaudRate, _portMetrics.getDataBits(), _portMetrics.getStopBits(), _portMetrics.getParity(), _portMetrics.getHandshake(), aReadTimeout, aWriteTimeout ) );
	}

	/**
	 * Opens the {@link TtyPort} instance.
	 * 
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public void open( int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( new TtyPortMetrics( _portMetrics.getBaudRate(), _portMetrics.getDataBits(), _portMetrics.getStopBits(), _portMetrics.getParity(), _portMetrics.getHandshake(), aReadTimeout, aWriteTimeout ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitSequence( Sequence aSequence ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot receive a segment from port <" + _alias + "> (" + _name + ") as the connection is in status <" + getConnectionStatus() + ">!" );
		}
		_serialPort.writeBytes( aSequence.toBytes(), aSequence.getLength() );
		// ---
		//	_serialPort.getOutputStream().write( aSequence.toBytes(), 0, aSequence.getLength() );
		//	_serialPort.getOutputStream().flush();
		// ---
		//	for ( byte eByte : aSequence.toBytes() ) {
		//		int unsignedIntFromLittleEndianBytes = NumericalUtility.toUnsignedIntFromLittleEndianBytes( new byte[] { eByte } );
		//		_serialPort.getOutputStream().write( unsignedIntFromLittleEndianBytes );
		//	}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveBytes( byte[] aBuffer, int aOffset, int aLength ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot receive a segment as the connection is in status <" + getConnectionStatus() + ">!" );
		}
		final int theLength = _serialPort.readBytes( aBuffer, aLength, aOffset );
		if ( theLength != aLength ) {
			throw new IOException( "Cannot only read as many as <" + theLength + "> bytes instead of the expected <" + aLength + "> bytes from port <" + _alias + "> (" + _name + ")" + ( !isOpened() ? ", the port status is <" + getConnectionStatus() + ">, maybe it was not opened or closed too eraly" : "" ) + "!" );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() {
		return _serialPort.bytesAvailable();
	}

	@Override
	public void flush() throws IOException {
		_serialPort.flushIOBuffers();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort withOpen( TtyPortMetrics aConnection ) throws IOException {
		open( aConnection );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( aBaudRate, aDataBits, aStopBits, aParity, aHandshake, aReadTimeout, aWriteTimeout );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( aBaudRate, aDataBits, aStopBits, aParity, aHandshake, aReadTimeout, aWriteTimeout );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance. Timeouts are
	 * disabled.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake ) throws IOException {
		open( aBaudRate, aDataBits, aStopBits, aParity, aHandshake );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance. Timeouts are
	 * disabled.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * @param aHandshake The {@link Handshake} be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity, Handshake aHandshake ) throws IOException {
		open( aBaudRate, aDataBits, aStopBits, aParity, aHandshake );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance. Timeouts are
	 * disabled. The underlying system's implementation or other implementation
	 * specific handshaking is used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( int aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity ) throws IOException {
		open( aBaudRate, aDataBits, aStopBits, aParity );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance. Timeouts are
	 * disabled. The underlying system's implementation or other implementation
	 * specific handshaking is used ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aDataBits The data bits to be set.
	 * @param aStopBits The {@link StopBits} to be set.
	 * @param aParity The {@link Parity} be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( BaudRate aBaudRate, int aDataBits, StopBits aStopBits, Parity aParity ) throws IOException {
		open( aBaudRate, aDataBits, aStopBits, aParity );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance. Timeouts are
	 * disabled. The underlying system's implementation or other implementation
	 * specific handshaking, stop bits and parity are used
	 * ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( int aBaudRate ) throws IOException {
		open( aBaudRate );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance. Timeouts are
	 * disabled. The underlying system's implementation or other implementation
	 * specific handshaking, stop bits and parity are used
	 * ({@link Handshake#AUTO}).
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( BaudRate aBaudRate ) throws IOException {
		open( aBaudRate );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The {@link BaudRate} to be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( BaudRate aBaudRate, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( aBaudRate, aReadTimeout, aWriteTimeout );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance.
	 * 
	 * @param aBaudRate The baud rate to be set.
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( int aBaudRate, int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( aBaudRate, aReadTimeout, aWriteTimeout );
		return this;
	}

	/**
	 * Builder method for opening a {@link TtyPort} instance.
	 * 
	 * @param aReadTimeout The read timeout to be set.
	 * @param aWriteTimeout The write timeout to be set.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing the {@link Port}
	 *         caused problems.
	 */
	public TtyPort withOpen( int aReadTimeout, int aWriteTimeout ) throws IOException {
		open( aReadTimeout, aWriteTimeout );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort withOpenUnchecked() {
		openUnchecked();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPort withOpenUnchecked( TtyPortMetrics aPortMetrics ) {
		openUnchecked( aPortMetrics );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TtyPortMetrics getPortMetrics() {
		return _portMetrics;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		if ( !_serialPort.isOpen() && super.isOpened() ) {
			super.closeQuietly();
		}
		return super.isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		super.close();
		synchronized ( this ) {
			notifyAll();
		}
		synchronized ( _serialPort ) {
			try {
				_serialPort.flushIOBuffers();
			}
			finally {
				_serialPort.removeDataListener();
				_serialPort.notifyAll();
				_serialPort.closePort();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getInputStream() {
		return _serialPort.getInputStream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TimeoutInputStream getInputStream( long aTimeoutMillis ) {
		return SerialUtility.createTimeoutInputStream( _serialPort.getInputStream(), aTimeoutMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputStream getOutputStream() {
		return _serialPort.getOutputStream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", portMetrics=" + _portMetrics + ", name=" + _name + ", description=" + _description + "]";
	}
}
